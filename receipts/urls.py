from django.urls import path
from receipts.views import (
    list_Receipt,
    create_Receipt,
    account_list,
    category_list,
    create_Category,
    create_Account
    )


urlpatterns = [
    path("", list_Receipt, name="home"),
    path("create/", create_Receipt, name="create_receipt"),
    path("categories/", category_list, name="category_list"),
    path("accounts/", account_list, name="account_list"),
    path("categories/create/", create_Category, name="create_category"),
    path("accounts/create/", create_Account, name="create_account")
]
